const mongoose = require('mongoose');

const bookSchema = mongoose.Schema({
    _id: Number,
    title: String,
    author: String,
    pages: [String]
});

module.exports = mongoose.model('Book', bookSchema);
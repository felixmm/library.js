const controllers = require("./controllers");
const helpers = require('./helpers');
const CONTENTTYPE = {'Content-Type': 'application/json'}

function onRequest(req, res) {

    // This favicon request ...
    if (req.url === '/favicon.ico') {
        res.writeHead(200, {'Content-Type': 'image/x-icon'} );
        res.end();
        return;
    }

    let response = {};

    // Only GETs
    if(req.method !== "GET") {
        
        helpers.respond(res, 500, response);
        return;
    }

    let route = helpers.getRoute(req.url);
    
    // Defaults
    let controller;
    let action;
    let id;
    let page;

    // Defaults
    if(route[0] === undefined || route[0] === '') {
        controller = "books";
        action = "home";
        id = null;
        page = 0;
    }
    else {
        // Requesting a specific controller
        controller = route[0];

        // Requesting either an item or another action
        if(route[1] === undefined || route[1] === '') {
            action = "home";
        }
        else if(!isNaN(route[1])) {
            action = "get";
            id = route[1];
        }
        else {
            action = route[1];
        }

        // An ID, if any
        if(route[2] !== undefined) {
            id = route[1];
        }
    }

    // Finding the resource requested
    let registeredControllers = Object.keys(controllers);

    let controllerFound = registeredControllers.filter(function(c) {
        return controllers[c].name === controller;
    }).map(function(c){ return controllers[c] });

    // Not found?
    if(controllerFound[0] === undefined) {

        helpers.respond(res, 404, response);
        return;
    }

    controllerFound = controllerFound[0];

    switch(action) {
        case "home":
            controllerFound.home(req, res);
        break;

        case "get":
            controllerFound.get(req, res, id);
        break;
    }
}

// Server configuration
const http = require("http");
const server = http.createServer(onRequest);
const port = 13370;
server.listen(port)
console.log("Listening to localhost:" + port);
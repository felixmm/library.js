const helpers = {
    
    getRoute: (url) => {
        return url.split("/").slice(1, url.length);
    },

    respond: (res, status, data) => {
        res.writeHead(status, 
            {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            }
        );
        res.write(JSON.stringify(data));
        res.end();
    }

};

module.exports = helpers;
# Library.js

Hello and welcome. Here we'll be doing a simple web API that handles books from a libary, hence the name being **Library.js**. We'll be using ~~ASPNET MVC~~ ... wait, no, we'll be using ~~Ruby on Rails~~ ... no? ~~Express~~? ... alright

![Fine, I'll do it myself](https://giant.gfycat.com/GrossDentalIlladopsis.gif)

# Tools we'll be using

We'll be using [Node.js](https://nodejs.org/) for all our processing needs and [MongoDB](https://www.mongodb.com/) as our database.

As for packages, we'll use:

- [Mongoose](http://mongoosejs.com/) as an object modeler and entry point for our database.

- [Nodemon](https://github.com/remy/nodemon) for development, to make be able to preview without manually restarting the node server.

# How to run Library.js

To run **library.js** you'll need to install [Node.js](https://nodejs.org/). After, using a terminal navigate to the folder where **library.js** is and  install all dependencies with:

```
npm install
```

To configure the database you'll need to install [mongodb](https://www.mongodb.com/). Configure your connection url to your mongo database in **db\configuration.js** file. This url should have the name for your database included. Ex:

``` javascript
const configuration = {
    database: "mongodb://localhost:27017/library"
};
``` 

We've provided an initial database seed for testing in **db\seed_database.js** file. After your connection to the database is configured, run the following command to add an initial set of books to your database:

``` 
node db\seed_database.js
```

After everything's set, in the root folder run either

```
npm start
```
or
```
npx nodemon
```

If you have the nodemon package installed. Either of these commands will start the server and you should see in the terminal

```
Listening to localhost:13370
```

Or your configured port if you happened to change it.

# Design Choices and Conventions

## General Decisions

This implementation only has one Book Controller since having a page without a book doesn't really work and brings more headaches than what it's worth.

Only JSON format will be delivered since it's widely used for this purpose and it's not intended for end users to receive and manage it as it. It;s up to each application that consumes this API to format and present the information as it choses. 

## MVC

We'll make this API following the Model View Controller patern, where we have our models, in our case defined by the Mongoose schemas and store in our database. Our views that in this case are non existant, anyone that consumes this API displays the data as they see fit, and the controllers that take the data, format it, verify it, etc and pass ir forward.

## Repository Pattern

Our database calls are encapsulated inside **database.js** file, which lets us retreive and handle the data without having to change how our controllers interact with it. As long as it's returned the same way it doesn't matter where it came from.

## Convetions

Given that we'll be making a bare bones web framework we'll be taking some design choices and adding some conventions to Library.js. These will be:

- Following the specifications provided, only **GET** requests are supported.

- Data will be returned in **JSON** format by default.

- All controllers must have a **home** function which will be the default, and a **get** function that will generally find a single item.

- All controllers must have a **name** string which will correspond with route used to call them.

- Route convention will be **/controller/action/id** where the action if not given will be **home** and when an ID is passed in the way of "/books/2" for example, the **get** action will be used and passed an ID of 2. Routes can also be called in their entirety, ex. "/books/home" and "/books/get/2".

- An exception to the rule above is the implementation to our Books Controller that also allows for a specific page to be requested. This is specified in the API calls section bellow.

# API Calls

The port in which the API runs can be configured in the **server.js** file near the bottom. Currently it's configured to run in port **13370**. The available calls for this API are:

## GET: /Books/
Returns the list of books available. This call doesn't return the book's pages.

Return structure:
``` json
[
    {
        "_id": 17,
        "title": "The Lord of the Rings: The Fellowship of the Ring",
        "author": "Gay Horne"
    },
    {
        "_id": 18,
        "title": "The Lord of the Rings: The Two Towers",
        "author": "Heath Dominguez"
    }
]
```

## GET: /Books/1
Returns a single book specified by the id passed. This call returns the book information with its pages included.

Return structure:
``` json
{
    "pages": [
        "page 1",
        "page 2",
        "page 3",
        ...
    ],
    "_id": 17,
    "title": "The Lord of the Rings: The Fellowship of the Ring",
    "author": "Gay Horne"
}
```
## GET: /Books/1/Page/5

Returns a single book specified by the id passed, and with only the page requested, if it's between the specific book pages. If a page is requestedd that exceeds those of the current book, all pages will be returned.

Return structure:
``` json
{
    "pages": [
        "page 5"
    ],
    "_id": 17,
    "title": "The Lord of the Rings: The Fellowship of the Ring",
    "author": "Gay Horne"
}
```

## Errors

If a bad request is made the API will return an empty result with HTTP status 500, and if a request for an item not available is made it'll return an empty result witl HTTP status 404.

# Future Improvements

The API was structured in a way that enables further improvements to be made. If more requests are desired they can be implemented by adding new controllers to the **controllers** folder and exporting them in the **index.js** file inside that folder. If they follow the conventions stated for this API they'll be available right away.

If another data source other than our database is needed, that can be incorporated in the **database.js** file withing the current functions available or create new ones. Since we're using a repository pattern our API only cares that the **database.js** returns the information requested, indiferent from where it's getting it, so this could be a mix of one or more databases, other API, text files, etc.

# Conclusions

As stated before, this is a bare bones, not production ready at all web framework. Its responses are very simple, no other allowed HTTP request other than GET calls, etc. But given what was requested, the nature of the request and time limit, this is what I've come up with.

When I was requested to do this task I thought that it would be a simple web API similar to any other tutorial on "How to use this web framework", but with the constraint of not being able to use **any** web framework whatsoever things got spicy. 

I initially planned on doing it with ASPNET MVC since I've been working with .NET for the last few years, but given the specifications of the request I decided to go with Node.js since it was conceived to be a way to handle server side requests for web processes. Granted I haven't really worked with Node.js apart from tutorials and side projects here and there, and I really learned a lot during the development of this API.
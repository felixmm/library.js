const mongoose = require('mongoose');
const configuration = require('./configuration');
const Books = require('../models/bookSchema');

mongoose.connect(configuration.database , { useNewUrlParser: true });
const db = mongoose.connection;

const database = {
    list: (clk) => {
        
        Books.find({}, { pages: 0 }, (err, res) => {
            clk(err, res);
        })
        .catch(err => {
            console.log(err);
        });
    },

    find: (id, clk) => {
        
        Books.findById(id, (err, res) => {
            clk(err, res);
        })
        .catch(err => {
            console.log(err);
        });
    }
}

module.exports = database;
const database = require('../db/database');
const helpers = require('../helpers');

const bookController = {
    name: "books",
    home: (req, res) => {
        console.log("GET /books");
        database.list((err, books) => {
            if (err) {
                helpers.respond(res, 404, { error: "Books not found" });
            }
            else {
                helpers.respond(res, 200, books);
            }
        });
    },

    get: (req, res, id) => {

        let route = helpers.getRoute(req.url);
        let pageIndex = -1;

        if(route[2] !== undefined
        && route[2].toUpperCase() === 'page'.toUpperCase()
        && route[3] !== undefined
        && !isNaN(route[3])) {
            console.log("GET /books/" + id + '/page/' + route[3]);
            pageIndex = (+route[3]) - 1; // Index of the page
        }
        else {
            console.log("GET /books/" + id);
        }

        database.find(id, (err, book) => {
            if(err) {
                helpers.respond(res, 404, { error: "Book not found"});
            }
            else {
                if(pageIndex >= 0 && pageIndex < book.pages.length) {
                    book.pages = book.pages.filter((value, index, array) => { return index === pageIndex });
                    helpers.respond(res, 200, book);
                }
                else {
                    helpers.respond(res, 200, book);
                }
            }
        });
    }
};

module.exports = bookController;